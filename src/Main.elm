module Main exposing (main)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onSubmit, onClick, onInput, keyCode, on)
import Json.Decode as Json


main =
    Html.program
        { init = ( initialModel, Cmd.none )
        , update = update
        , subscriptions = \_ -> Sub.none
        , view = view
        }


type Card
    = NameForm
    | PhoneForm
    | Greeting


type alias Model =
    { currentCard : Card
    , previousCards : List Card
    , nextCards : List Card
    , name : Maybe String
    , phone : Maybe String
    }


initialModel =
    { currentCard = NameForm
    , previousCards = []
    , nextCards = [ PhoneForm, Greeting ]
    , name = Nothing
    , phone = Nothing
    }


type Message
    = Back
    | Forward
    | KeyPress Int
    | NameChange String
    | PhoneChange String
    | Noop


update : Message -> Model -> ( Model, Cmd Message )
update message model =
    let
        next =
            case List.head model.nextCards of
                Just nextCard ->
                    ( { model
                        | currentCard = nextCard
                        , previousCards = model.currentCard :: model.previousCards
                        , nextCards = List.drop 1 model.nextCards
                      }
                    , Cmd.none
                    )

                Nothing ->
                    ( model, Cmd.none )

        same =
            ( model, Cmd.none )
    in
        case message of
            Noop ->
                same

            KeyPress key ->
                if key == 13 then
                    next
                else
                    same

            Back ->
                case List.head model.previousCards of
                    Just prevCard ->
                        ( { model
                            | currentCard = prevCard
                            , previousCards = List.drop 1 model.previousCards
                            , nextCards = model.currentCard :: model.nextCards
                          }
                        , Cmd.none
                        )

                    Nothing ->
                        same

            Forward ->
                next

            NameChange newName ->
                ( { model | name = nothingString newName }, Cmd.none )

            PhoneChange newPhone ->
                ( { model | phone = nothingString newPhone }, Cmd.none )


nothingString : String -> Maybe String
nothingString string =
    if String.isEmpty string then
        Nothing
    else
        Just string


view model =
    div []
        [ header []
            [ div [ class "logo" ] [ text "Eave" ] ]
        , main_ []
            [ div [ class "arrow-container" ]
                [ div (arrowButtonProperties model.previousCards "fa-arrow-left" Back) [] ]
            , section [ class "card" ] [ card model ]
            , div [ class "arrow-container" ]
                [ div (arrowButtonProperties model.nextCards "fa-arrow-right" Forward) [] ]
            ]
        , footer []
            []
        ]


arrowButtonProperties : List Card -> String -> Message -> List (Attribute Message)
arrowButtonProperties list arrowClass message =
    if List.isEmpty list then
        []
    else
        [ class ("arrow fa " ++ arrowClass), onClick message ]


onKeyPress : (Int -> msg) -> Attribute msg
onKeyPress tagger =
    on "keypress" (Json.map tagger keyCode)


card model =
    case model.currentCard of
        NameForm ->
            Html.form [ onSubmit Noop ]
                [ label [ for "name" ] [ text "Hi there! What's your name?" ]
                , input [ type_ "text", id "name", value (Maybe.withDefault "" model.name), onInput NameChange, onKeyPress KeyPress ] []
                ]

        PhoneForm ->
            Html.form [ onSubmit Noop ]
                [ label [ for "name" ] [ text "What about your phone number?" ]
                , input [ type_ "text", id "name", value (Maybe.withDefault "" model.phone), onInput PhoneChange, onKeyPress KeyPress ] []
                ]

        Greeting ->
            case model.name of
                Just name ->
                    case model.phone of
                        Just phone ->
                            Html.text ("Hi, " ++ name ++ "! Your phone number is " ++ phone ++ ".")

                        Nothing ->
                            Html.text ("Hi, " ++ name ++ ". I don't have your phone number yet!")

                Nothing ->
                    case model.phone of
                        Just phone ->
                            Html.text ("Your phone number is " ++ phone ++ ", but I don't know your name!")

                        Nothing ->
                            Html.text "I don't have your name OR phone number, stranger!"
