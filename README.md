# Eave Take-Home Exam

Hello! Thank you for taking the time to review my coding assignment.

As you will learn (or may already know from my resume), I'm very interested in a front-end programming language called Elm. You can learn about it [here](http://elm-lang.org/).

I decided to go ahead and model this problem in Elm, because it seemed like a good fit. I'm happy to talk over the code with you. In the mean time, here's what you'll need to take a look at what I've made.

### Requirements
+ npm (the version shouldn't matter)

### Steps
+ first, install Elm! `npm install -g elm@latest`
+ next, run my build script: `./scripts/build`. when prompted, answer "Y" to install the necessary Elm packages.
+ finally, open the file `index.html`. That's all!

Please do not hesitate to contact me if you run into any issues! Thank you again.

Best,
Noah
